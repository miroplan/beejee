<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="http://bootstrap-3.ru/assets/ico/favicon.ico">

    <title><?php print isset($title) ? $title : $_SERVER['HTTP_HOST']; ?></title>
     
    <base href="/">

    <!-- Bootstrap core CSS -->
    <link href="http://bootstrap-3.ru/dist/css/bootstrap.min.css" rel="stylesheet">

    <link href="css/main.css" rel="stylesheet">

    <?php if (isset($css) && is_array($css)) : ?>
    
    <?php foreach ($css as $val) : ?>
  
    <link href="css/<?php print $val; ?>.css" rel="stylesheet">
  
    <?php endforeach; ?>

    <?php endif; ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <script src="http://bootstrap-3.ru/dist/js/bootstrap.min.js"></script>
    
    <?php if (isset($js) && is_array($js)) : ?>
    
    <?php foreach ($js as $val) : ?>
  
    <script type="text/javascript" src="js/<?php print $val; ?>.js"></script>
  
    <?php endforeach; ?>

    <?php endif; ?>

  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">Задачи</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav" style="float:right;">
            <li><a href="tasks/new">Добавить задачу</a></li>
            

<?php if (isset($_SESSION['user_id'])) : ?>

<li><a href="/admin" >Админ</a></li> 

<li><a href="/signout" >Выход</a></li>

<?php else : ?>

<li><a href="signin" >Вход</a></li>

<?php endif; ?>
           
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container"><?php print $data; ?></div><!-- /.container -->

  </body>
</html>