<?php

class admin extends controller {

public function __construct () {

$_REQUEST['offset'] = isset($_REQUEST['offset']) ? $_REQUEST['offset'] : 1;

}
 
public function index (&$req) {

if (isset($_SESSION['user_id'])) {

return(self::view([

'url' => 'main',

'title' => 'Админ',

'js' => ['admin'],

'css' => ['admin'],

'data' => self::view([

'url' => __CLASS__.'/main',

'tasks' => m_tasks::tasks()

])

]));

} else func::redirect('/signin');

}



public function accept (&$req) {

$result = app::$db->query("

UPDATE tasks SET task_status = 1 WHERE task_id = {$req['id']}

");

if ($result) {

return self::view([

'url' => __CLASS__.'/data',

'tasks' => m_tasks::tasks($req)

]);

} else {throw new Exception("Error Processing Request",400);}

}

public function reject (&$req) {

$result = app::$db->query("

UPDATE tasks SET task_status = 0 WHERE task_id = {$req['id']}

");

if ($result) {

return self::view([

'url' => __CLASS__.'/data',

'tasks' => m_tasks::tasks($req)

]);

} else {throw new Exception("Error Processing Request",400);}
	
}



public function edit (&$req) {

$result = app::$db->query("

UPDATE tasks SET 

task_name = '{$req['name']}', 

task_email = '{$req['email']}',

task_text = '{$req['text']}'

WHERE task_id = {$req['id']}

");

if ($result) {

return self::view([

'url' => __CLASS__.'/data',

'tasks' => m_tasks::tasks($req)

]);

} else {throw new Exception("Error Processing Request",400);}
	
}


}

