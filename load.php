<?php

foreach (glob(__DIR__.'/lib/*.php') as $file) include('lib/'.basename($file));

foreach (glob(__DIR__.'/system/*.php') as $file) include('system/'.basename($file));

function __autoload ($class) {

if (file_exists(__DIR__.'/models/'.$class.'.php')) include('models/'.$class.'.php');

if (file_exists(__DIR__.'/controllers/'.$class.'.php')) include('controllers/'.$class.'.php');

}

?>