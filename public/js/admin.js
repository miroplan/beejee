
var admin = {

accept: function (id) {

$.ajax({

url: '/admin/accept',

method: 'POST',

data: {id},

success: function (data) {

$('#data').html(data);

},

error: function (xhr,status,text) {}

});

},

reject: function (id) {

$.ajax({

url: '/admin/reject',

method: 'POST',

data: {id},

success: function (data) {

$('#data').html(data);

},

error: function (xhr,status,text) {}

});

},


edit: function (id) {

var name = document.getElementById('edit_name');

name.value = document.getElementById('name' + id).textContent;

var email = document.getElementById('edit_email');

email.value = document.getElementById('email' + id).textContent;

var text = document.getElementById('edit_text');

text.value = document.getElementById('text' + id).textContent;

$('#myModal').modal('show');

$('#edit_save').click(function (e) {

if (!edit.reportValidity()) return false;

$.ajax({

url: '/admin/edit',

method: 'POST',

data: {

id:	id,

name: encodeURIComponent(name.value), 

email: encodeURIComponent(email.value),

text: encodeURIComponent(text.value)

},

success: function (data) {

$('#data').html(data);

$('#myModal').modal('hide');

},

error: function (xhr,status,text) {}

});

});

}



};