<?php 

class app {

protected $class;

public static $db = false;

protected $map = [

'/' => 'tasks',

'/signin' => 'main/signin',

'/signout' => 'main/signout',

'/sign_check' => 'main/sign_check',

'/tasks/new' => 'tasks/new',

'/tasks/add' => 'tasks/add',

'/tasks/sort' => 'tasks/sort',

'/admin' => 'admin/index',

'/admin/edit' => 'admin/edit',

'/admin/accept' => 'admin/accept',

'/admin/reject' => 'admin/reject'

];

function __construct () {

if (!self::$db) self::$db = new db();

}

public function run (&$app) {

$_REQUEST = func::query_filter($_REQUEST);

$req = array_key_exists('REDIRECT_URL',$_SERVER) ? $_SERVER['REDIRECT_URL'] : '/';

if (array_key_exists($req,$this->map)) {

$url = explode('/',$this->map[$req]);

$this->class = new $url[0]($app);

print $this->class->{isset($url[1]) ? $url[1] : 'index'}($_REQUEST);

} else {

print template::view(['url' => '404']);

}

}

function __destruct () {

if (self::$db) self::$db->close();

unset($this->class);

}


}

?>