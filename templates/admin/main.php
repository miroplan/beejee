<?php if ($tasks['sum']) : ?>

<table class="table">

<thead>
        <tr>
          <th><b>Имя</b></th>
          <th><b>Email</b></th>
          <th><b>Задача</b></th>
          <th><b>Статус</b></th>
          <th><b>Действия</b></th>        
        </tr>

      </thead>

<tbody id="data"><?php include('data.php'); ?></tbody>

      <?php if ($tasks['sum'] > 3) : ?>

      <tfoot>

      <tr>  
       
      <td colspan="5" align="center">       

      <ul class="pagination">

      <?php for ($i = 1; $i <= ceil($tasks['sum'] / 3); $i++) : ?>

        <li <?php if ($_REQUEST['offset'] == $i) : ?>class="active"<?php endif; ?>><a href="/admin?offset=<?php print $i; ?>"><?php print $i; ?></a></li>

      <?php endfor; ?>

      </ul>

      </td>

      </tr> 

      </tfoot>

      <?php endif; ?>

</table>

<?php else : ?>

<center><strong>Нет сообщений..</strong></center>

<?php endif; ?>

<div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Редактировать</h4>
      </div>
      <div class="modal-body">
 
<form id="edit" onsubmit="return false;">

<div class="form-group">	
	<label>Имя</label>
	<input type="text" id="edit_name" class="form-control" minlength="3" maxlength="20" required>
</div>

<div class="form-group">
	<label>Email</label>
	<input id="edit_email" type="email" class="form-control" minlength="5" maxlength="20" required>
</div>

<div class="form-group">
	<label>Текст</label>
	<textarea id="edit_text" name="text" class="form-control" minlength="5" maxlength="2000" required></textarea>
</div>

<div><button id="edit_save" type="submit" class="btn btn-primary">Сохранить</button></div>

</form>

</div>

</div>

</div>

</div>
