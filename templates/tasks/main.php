<?php if ($tasks['sum']) : ?>

<table class="table">
      <thead>
        <tr>
          <th><a id="sort_name" >Имя</a></th>
          <th><a id="sort_email" >Email</a></th>
          <th><b>Задача</b></th>
          <th><a id="sort_status" >Статус</a></th>        
        </tr>
      </thead>
 
      <tbody id="data"><?php include('data.php'); ?></tbody>

      <?php if ($tasks['sum'] > 3) : ?>

      <tfoot>

      <tr>  
       
      <td colspan="4" align="center">       

      <ul class="pagination">

      <?php for ($i = 1; $i <= ceil($tasks['sum'] / 3); $i++) : ?>

        <li <?php if ($_REQUEST['offset'] == $i) : ?>class="active"<?php endif; ?>><a href="?offset=<?php print $i; ?>"><?php print $i; ?></a></li>

      <?php endfor; ?>

      </ul>

      </td>

      </tr> 

    </tfoot>

    <?php endif; ?>

    </table>

<script type="text/javascript">
  
$(document).ready(function(e){

$('#sort_name').click(function(e){tasks.sort('name');});

$('#sort_email').click(function(e){tasks.sort('email');});

$('#sort_status').click(function(e){tasks.sort('status');});

});

</script>

<?php else : ?>

<center><strong>Нет сообщений..</strong></center>

<?php endif; ?>

