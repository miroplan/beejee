<div style="width: 50%; margin: 10px auto;">

<form id="task" action="tasks/set" enctype="application/x-www-form-urlencoded" method="post" >

<div class="form-group">
<label>Имя</label>
<input type="text" name="name" class="form-control" placeholder="Имя" maxlength="50" minlength="3" required>
</div>

<div class="form-group">
<label>Email</label>
<input type="email" name="email" class="form-control" minlength="5" maxlength="50" placeholder="Email" required>
</div>

<div class="form-group">
<label>Задача</label>
<textarea name="task" class="form-control" maxlength="10000" placeholder="Задача" minlength="5" required></textarea>
</div>
  
<div class="form-group"><button type="button" id="button_preview" class="btn btn-default btn-block">Предосмотр</button>&nbsp;&nbsp;<button type="submit" id="button_add" class="btn btn-default btn-block">Отправить</button></div>

</form>

</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Предосмотр</h4>
      </div>
      
      <div class="modal-body">
        
<div>Имя:&nbsp;<b><a id="preview_name"></a></b></div>

<div>Email:&nbsp;<b id="preview_email"></b></div>

<div>Задача:&nbsp;<b id="preview_task"></b></div>

      </div>
      
      </div>
  
    </div>

  </div>

<script type="text/javascript">
  
$(document).ready(function(e){

$('#button_add').click(tasks.add);

$('#button_preview').click(tasks.preview);

$('#task').submit(function(e){return false;});

});

</script>