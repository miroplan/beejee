<?php

class tasks extends controller {
 
public function __construct () {

$_REQUEST['offset'] = isset($_REQUEST['offset']) ? intval($_REQUEST['offset']) : 1;

}

public function index (&$req) {

$tasks = m_tasks::tasks();

if ($tasks) {

return(self::view([

'url' => 'main',

'title' => 'Задачник',

'js' => ['tasks'],

'css' => ['tasks'],

'data' => self::view([

'url' => __CLASS__.'/main',

'tasks' => $tasks

])

]));

} else throw new Exception("No data.",400);

}


public function sort (&$req) {

return(self::view([

'url' => __CLASS__.'/data',

'tasks' => m_tasks::sort($req)

]));
	
}


public function new () {

return(self::view([

'url' => 'main',

'title' => 'Добавить задачу',

'css' => ['tasks'],

'js' => ['tasks'],

'data' => self::view([

'url' => __CLASS__.'/new'

])

]));

}


public function add (&$req) {

if (m_tasks::add($req)) return true;

else throw new Exception("Запись не добавлена.",400);

}

public function __destruct () {}


}
