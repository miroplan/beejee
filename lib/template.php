<?php  

abstract class template {

public static function view (array $data=[]) {

extract($data);

if (file_exists(TEMPLATE.'/'.$url.'.php')) {

ob_start();

require(TEMPLATE.'/'.$url.'.php');

return(ob_get_clean());

} else return null;

}

}

?>