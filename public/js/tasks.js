var tasks = {

order: 'ASC',

preview: function () {

preview_name.textContent = task.elements.name.value.length ? task.elements.name.value : '?';

preview_email.textContent = task.elements.email.value.length ? task.elements.email.value : '?';

preview_task.textContent = task.elements.task.value.length ? task.elements.task.value : '?';

$('#myModal').modal('show');

},


add: function (e) {

if (!task.reportValidity()) return false;

e.target.disabled = true;

$.ajax({

url: 'tasks/add',

method: 'POST',

data: {

name: encodeURIComponent(task.elements.name.value),

email: encodeURIComponent(task.elements.email.value),

task: encodeURIComponent(task.elements.task.value)

},

success: function (data) {document.location.assign("/");},

error: function (xhr,status,text) {}

});


},


sort: function (sort) {

$.ajax({

url: 'tasks/sort',

method: 'POST',

data: {

sort: sort,

order: tasks.order = tasks.order == 'ASC' ? 'DESC' : 'ASC'

},

success: function (data) {

$('#data').html(data);

},

error: function (xhr,status,text) {}

});


}


};
