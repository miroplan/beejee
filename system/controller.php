<?php  

class controller extends template {

public function __construct () {}

public function __call($name,$args) {
    
exit('Error: '.get_called_class().' not exist method <b>'.$name.'</b>');  
        
}
    
public static function __callStatic($name,$args) {
    
exit('Error: '.get_called_class().' not exist static method <b>'.$name.'</b>');
        
}

public function __get($name) {

exit($name);

}

public function __set($name,$value) {

exit($name.': '.$value);
	
}

public function __isset($name) {

exit($name);

}

public function __unset($name) {

exit($name);

}

public function __destruct () {}
	
}

?>